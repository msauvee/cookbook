#!/bin/bash

resetRAMDisk() {
    name="ramdisk"
    device=$(hdiutil info | grep ${name} | awk '{print $1}')
    echo "device : ${device}"
    if [ ! -z "${device}" ]; then
        echo "hdiutil unmount ${device}"
        hdiutil unmount ${device}
    fi
    diskutil erasevolume HFS+ ${name} `hdiutil attach -nomount ram://16777216`
}

resetRAMDisk